import sys
import time
import copy
import math
from settings import *

MAX_VALUE = 100


class GameState:
    # Initializer for the Connect4 GameState
    # Board is initialized to size width*height
    def __init__(self, rows, cols):
        self.__rows = rows        # number of rows in the board
        self.__cols = cols        # number of columns in the board
        self.__pieces = [0]*cols  # __pieces[c] = number of pieces in a column c
        self.__player = 0         # the current player to move, 0 = Player One, 1 = Player Two
        self.__board = [[PLAYER_NONE]*cols for _ in range(rows)]
        self.__eval_table = [[0]*cols for _ in range(rows)]
        self.fill_eval_table()

    # performs the given move, putting the piece into the appropriate column and swapping the player
    def do_move(self, move):
        if not self.is_legal(move):
            print("DOING ILLEGAL MOVE")
            sys.exit()
        self.__board[self.pieces(move)][move] = self.player_to_move()
        self.__pieces[move] += 1
        self.__player = (self.__player + 1) % 2

    # undoes the given move, removing the piece from the appropriate column and swapping the player
    def undo_move(self, move):
        self.__board[self.pieces(move) - 1][move] = PLAYER_NONE
        self.__pieces[move] -= 1
        self.__player = (self.__player + 1) % 2
    
    # some getter functions that you probably won't need to modify
    def get(self, r, c): return self.__board[r][c]     # piece type located at (r,c)

    def cols(self): return self.__cols                 # number of columns in board

    def rows(self): return self.__rows                 # number of rows in board

    def pieces(self, col): return self.__pieces[col]   # number of pieces in a given column

    def total_pieces(self): return sum(self.__pieces)  # total pieces on the board

    def player_to_move(self): return self.__player     # the player to move next

    # a move (placing a piece into a given column) is legal if the column isn't full
    def is_legal(self, move): return 0 <= move < self.cols() and self.pieces(move) < self.rows()

    # returns a list of legal moves at this state (which columns aren't full yet)
    def get_legal_moves(self): return [i for i in range(self.cols()) if self.is_legal(i)]

    def is_in_bounds(self, row, col): return 0 <= row < self.rows() and 0 <= col < self.cols()

    def get_eval_table(self, r, c): return self.__eval_table[r][c]

    def fill_eval_table(self):
        cols = self.cols()
        rows = self.rows()

        for row in range(0, rows):
            for col in range(0, cols):
                # check horizontal
                for i in range(1, 4):
                    if not self.is_in_bounds(row, col + i):
                        break
                else:
                    for i in range(0, 4):
                        self.__eval_table[row][col + i] += 1

                # check vertical
                for i in range(1, 4):
                    if not self.is_in_bounds(row + i, col):
                        break
                else:
                    for i in range(0, 4):
                        self.__eval_table[row + i][col] += 1

                # check diagonal up
                for i in range(1, 4):
                    if not self.is_in_bounds(row + i, col + i):
                        break
                else:
                    for i in range(0, 4):
                        self.__eval_table[row + i][col + i] += 1

                # check diagonal down
                for i in range(1, 4):
                    if not self.is_in_bounds(row - i, col + i):
                        break
                else:
                    for i in range(0, 4):
                        self.__eval_table[row - i][col + i] += 1

    # return number of sets of 3 pieces of the given player in a row that could potentially be a win in the future
    def threes(self, player):
        cols = self.cols()
        rows = self.rows()

        count = 0

        # check horizontal
        for row in range(0, rows):
            for col in range(0, cols - 3):
                current_piece = self.get(row, col)
                if current_piece == player:
                    for i in range(1, 3):
                        if self.get(row, col + i) != current_piece:
                            break
                    else:
                        count += 1

        # check vertical
        for row in range(0, rows - 3):
            for col in range(0, cols):
                current_piece = self.get(row, col)
                if current_piece == player:
                    for i in range(1, 3):
                        if self.get(row + i, col) != current_piece:
                            break
                    else:
                        count += 1

        # check diagonal up
        for row in range(0, rows - 3):
            for col in range(0, cols - 3):
                current_piece = self.get(row, col)
                if current_piece == player:
                    for i in range(1, 3):
                        if self.get(row + i, col + i) != current_piece:
                            break
                    else:
                        count += 1

        # check diagonal down
        for row in range(3, rows):
            for col in range(0, cols - 3):
                current_piece = self.get(row, col)
                if current_piece == player:
                    for i in range(1, 3):
                        if self.get(row - i, col + i) != current_piece:
                            break
                    else:
                        count += 1

        return count

    # Calculates a heuristic evaluation for the current GameState from the P.O.V. of the player to move
    def eval(self, player):
        # center_points = 0
        # for i in range(0, math.floor(self.cols() / 2)):
        #     center_points += self.pieces(i) * i + self.pieces(self.cols() - i - 1) * i + 1
        # if self.cols() % 2 == 1:
        #     center_points += self.pieces(math.floor(self.cols() / 2)) * math.floor(self.cols() / 2)  # center row
        opponent = (player + 1) % 2
        total_pieces_points = self.total_pieces()
        player_threes = self.threes(player)
        opponent_threes = self.threes(opponent)

        connected_points = 0
        for row in range(0, self.rows()):
            for col in range(0, self.cols()):
                if self.get(row, col) == player:
                    connected_points += self.get_eval_table(row, col)
                elif self.get(row, col) == opponent:
                    connected_points -= self.get_eval_table(row, col)

        if self.winner() == player:
            return MAX_VALUE - total_pieces_points
        elif self.winner() == opponent:
            return -MAX_VALUE + total_pieces_points
        else:
            return total_pieces_points + connected_points + player_threes - opponent_threes

    # Calculates whether or not there is a winner on the current board
    def winner(self):
        cols = self.cols()
        rows = self.rows()

        # check for horizontal win
        for row in range(0, rows):
            for col in range(0, cols - 3):
                current_piece = self.get(row, col)
                if current_piece == PLAYER_ONE or current_piece == PLAYER_TWO:
                    for i in range(1, 4):
                        if self.get(row, col + i) != current_piece:
                            break
                    else:
                        return PLAYER_ONE if current_piece == PLAYER_ONE else PLAYER_TWO

        # check for vertical win
        for row in range(0, rows - 3):
            for col in range(0, cols):
                current_piece = self.get(row, col)
                if current_piece == PLAYER_ONE or current_piece == PLAYER_TWO:
                    for i in range(1, 4):
                        if self.get(row + i, col) != current_piece:
                            break
                    else:
                        return PLAYER_ONE if current_piece == PLAYER_ONE else PLAYER_TWO

        # check for diagonal up win
        for row in range(0, rows - 3):
            for col in range(0, cols - 3):
                current_piece = self.get(row, col)
                if current_piece == PLAYER_ONE or current_piece == PLAYER_TWO:
                    for i in range(1, 4):
                        if self.get(row + i, col + i) != current_piece:
                            break
                    else:
                        return PLAYER_ONE if current_piece == PLAYER_ONE else PLAYER_TWO

        # check for diagonal down win
        for row in range(3, rows):
            for col in range(0, cols - 3):
                current_piece = self.get(row, col)
                if current_piece == PLAYER_ONE or current_piece == PLAYER_TWO:
                    for i in range(1, 4):
                        if self.get(row - i, col + i) != current_piece:
                            break
                    else:
                        return PLAYER_ONE if current_piece == PLAYER_ONE else PLAYER_TWO

        # check for draw
        for col in range(0, cols):
            if self.pieces(col) != rows:
                break
        else:
            return DRAW

        return PLAYER_NONE


class Player_AlphaBeta:
    # Constructor for the Player_AlphaBeta class
    def __init__(self, max_depth, time_limit):
        self.max_depth = max_depth if max_depth != 0 else sys.maxsize
        self.current_max_depth = 0
        self.time_limit = time_limit if time_limit != 0 else sys.maxsize
        self.time_start = 0
        self.time_elapsed_ms = 0
        self.best_move = 0
        self.temp_move = 0
        self.player = None
        self.ab_values = []

    # calculates the move to be performed by the AI at a given state
    def get_move(self, state):
        self.time_start = time.clock()
        self.player = state.player_to_move()
        self.id_alpha_beta(copy.deepcopy(state))
        return self.best_move

    # performs the alpha-beta pruning
    def alpha_beta(self, state, depth, alpha, beta, max_player):
        self.time_elapsed_ms = (time.clock() - self.time_start) * 1000

        if self.is_overtime():
            raise TimeoutException

        if self.is_terminal(state, depth):
            return state.eval(self.player)

        for move in state.get_legal_moves():
            state.do_move(move)
            value = self.alpha_beta(state, depth + 1, alpha, beta, not max_player)
            state.undo_move(move)
            if depth == 0:
                self.ab_values.append(value)
            if max_player and (value > alpha):
                if depth == 0:
                    self.temp_move = move
                alpha = value
            elif not max_player and (value < beta):
                beta = value
            if alpha >= beta:
                break
        return alpha if max_player else beta

    def id_alpha_beta(self, state):
        for d in range(1, self.max_depth + 1):
            try:
                self.ab_values = []
                self.current_max_depth = d
                # print("depth", self.current_max_depth)
                self.alpha_beta(copy.deepcopy(state), 0, -math.inf, math.inf, True)
                self.best_move = self.temp_move
                # print(self.ab_values)
            except TimeoutException:
                break

    def is_terminal(self, state, depth):
        return True if depth >= self.current_max_depth > 0 else state.winner() != PLAYER_NONE

    def is_overtime(self):
        return self.time_limit != 0 and self.time_elapsed_ms >= self.time_limit


class TimeoutException(Exception):
    pass
