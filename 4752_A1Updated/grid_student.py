import sys              # used for file reading
import math
from settings import *  # use a separate file for all the constant settings
import numpy
import heapq

# the class we will use to store the map, and make calls to path finding
class Grid:
    # set up all the default values for the frid and read in the map from a given file
    def __init__(self, filename):
        # 2D list that will hold all of the grid tile information 
        self.__grid = []
        self.__load_data(filename)
        self.__width, self.__height = len(self.__grid), len(self.__grid[0])
        self.connected_grid = numpy.zeros((self.width(), self.height(), 3))
        self.connected_map_compleat = False
        self.connected_map_filling = False
        self.fringe_s1 = []
        self.fringe_s2 = []
        self.fringe_s3 = []
        self.printedconnected = False
        self.fill_connected_maps()


        # loads the grid data from a given file name
    def __load_data(self, filename):
        # turn each line in the map file into a list of integers
        temp_grid = [list(map(int,line.strip())) for line in open(filename, 'r')]
        # transpose the input since we read it in as (y, x) 
        self.__grid = [list(i) for i in zip(*temp_grid)]

    # return the cost of a given action
    # note: this only works for actions in our LEGAL_ACTIONS defined set (8 directions)
    def __get_action_cost(self, action):
        return CARDINAL_COST if (action[0] == 0 or action[1] == 0) else DIAGONAL_COST 

    # returns the tile type of a given position
    def get(self, tile): return self.__grid[tile[0]][tile[1]]
    def width(self):     return self.__width
    def height(self):    return self.__height
    def connected_map_created(self): return

    def get_connected(self, tile, size):
        temp = self.connected_grid[tile[0]][tile[1]][size-1]
        return temp

    def set_connected_map_cell(self, tile, val, size):
        self.connected_grid[tile[0]][tile[1]][size-1] = val
        return
    def fill_connected_maps(self):
        #if (not (self.connected_map_compleat) and not(self.connected_map_filling)):
        connected_val =1
        self.fill_connected_map((0, 0), (0, 0), connected_val, 1)
        self.fill_connected_map((0, 0), (0, 0), connected_val, 2)
        self.fill_connected_map((0, 0), (0, 0), connected_val, 3)
        while(self.fringe_s1):
            # print("___new group s1___")
            startingval = self.fringe_s1.pop(0)
            connected_val += 1
            self.fill_connected_map(startingval, startingval, connected_val, 1)
        connected_val = 1
        while (self.fringe_s2):
            # print("___new group s2___")
            startingval = self.fringe_s2.pop(0)
            connected_val += 3
            self.fill_connected_map(startingval, startingval, connected_val, 2)
        connected_val = 1
        while (self.fringe_s3):
            # print("___new group s3___")
            startingval = self.fringe_s3.pop(0)
            connected_val += 3
            self.fill_connected_map(startingval, startingval, connected_val, 3)
        # print("connected map filled")
        # print(self.connected_grid[:][:][:])

    # Student TODO: Implement this function
    # returns true of an object of a given size can navigate from start to goal
    def is_connected(self, start, goal, size):
        try:
            self.get_connected(goal, size)
            if (self.get_connected(start, size) != self.get_connected(goal, size)) or self.get_connected(start, size) == -1 or self.get_connected(start, size) == 0 :
                return False
            # print("conneced val: ", self.get_connected(start, size))
            return True
        except IndexError:
            print("error val dose not exist", goal, " size : ", size)
            return False
        return False


    def is_in_map_range(self, x, y):
        if y>=0 and y<self.height() and x>=0 and x<self.width():
            return True
        else:
            return False

    def fill_connected_map(self, start_cell, current_cell, connected_val, size):
        current_cell_val = self.get(current_cell)
        # print("start of section: ", start_cell, "current cell: ", current_cell, "connect val: ", connected_val, "size: ", size)
        if (self.get_connected(current_cell, size) == 0) and self.get(start_cell) == self.get(current_cell):  # cell not filled and matched traine type
            conflict = False
            for blocksX in range(current_cell[0], current_cell[0] + size):  # shifts through row of goal block
                for blocksY in range(current_cell[1], current_cell[1] + size):  # shifts through columns of goal block
                    # T = goalcellX, goalcellY
                    # print("current check", T)
                    if ( self.is_in_map_range(blocksX, blocksY)):
                        # if((self.get(start_cell) != self.get((blocksX, blocksY)) )): #and self.get_connected(start_cell) == self.get_connected((start_cell[0]+blocksX-goal[0], start_cell[1]+blocksY-goal[1])))):  #TODO check if goalcell and startcell is equal to start block orgin (start)
                        # set conflicting value to fringe
                        if (size == 1):
                            self.fringe_s1.append((blocksX, blocksY))
                            # print("added val to fringe_s1 ", (blocksX, blocksY))
                            # conflict = True
                            # continue
                        elif (size == 2):
                            self.fringe_s2.append((blocksX, blocksY))
                            # print("added val to fringe_s2 ", (blocksX, blocksY))
                            # conflict = True
                            # continue
                        elif (size == 3):
                            self.fringe_s3.append((blocksX, blocksY))
                            # print("added val to fringe_s3 ", (blocksX, blocksY))
                            # conflict = True
                            # continue
                        if ((self.get(start_cell) != self.get((blocksX, blocksY)))):
                            conflict = True
                    else:
                        conflict = True
                    # elif(self.is_in_map_range(blocksX, blocksY) and blocksX == current_cell[1] + size-1 and blocksY == (current_cell[0] + size)-1):
                    #   self.set_connected_map_cell(current_cell, connected_val, size)  # fill cell
            # print("conflict: ", conflict)
            if(not conflict):
                self.set_connected_map_cell(current_cell, connected_val, size)
                # if(size>1):
                #     for blocksX in range(current_cell[0], current_cell[0] + size):  # shifts through row of goal block
                #         for blocksY in range(current_cell[1], current_cell[1] + size):
                #             self.set_connected_map_cell((blocksX, blocksY), connected_val, size)
                try:
                    self.move(start_cell, current_cell, connected_val, size)
                except IndexError:
                    print("something went wrong with filling conecceted map")
                    return
            else:
                # print("###########################")
                self.set_connected_map_cell(current_cell, -1, size)  #print("conflict- current cell: ", current_cell, " current val: ", connected_val,)  #
                # self.move(start_cell, current_cell, -1, size)
        elif self.get_connected(current_cell, size) == 0 and self.get(start_cell) != current_cell_val:
            if(size == 1 ):
                self.fringe_s1.append(current_cell)
                #print("added val to fringe_s1 ", current_cell)
            elif(size == 2 ):
                self.fringe_s2.append(current_cell)
                # print("added val to fringe_s2 ", current_cell)
            elif(size == 3):
                self.fringe_s3.append(current_cell)
                #print("added val to fringe_s3 ", current_cell)

            return
        return


    def move(self, start_cell, current_cell, connected_val, size):
        ##33##33print("move")
        current_x = current_cell[0]
        current_y = current_cell[1]
        if (self.is_in_map_range(current_x, current_y - 1)):# and self.get_connected((current_y - 1, current_x)) == 0):
            ##33print("move North leagle, tring it!")
            move_north = (current_x, current_y - 1)
            self.fill_connected_map(start_cell, move_north, connected_val, size)  # move North
            ##33print("moved North to ", move_north)
        if (self.is_in_map_range(current_x + 1, current_y) ): #and self.get_connected((current_y, current_x + 1)) == 0):
            ##33print("move East leagle, tring it!")
            move_east = (current_x + 1, current_y)
            self.fill_connected_map(start_cell, move_east, connected_val, size)  # move east
            ##33print("moved east to ", move_east)
        if (self.is_in_map_range(current_x, current_y + 1) ):#and self.get_connected((current_y + 1, current_x)) == 0):
            ##33print("move South leagle, tring it!")
            move_south = (current_x, current_y + 1)
            self.fill_connected_map(start_cell, move_south, connected_val, size)  # move south
            ##33print("moved South to ", move_south)
        if (self.is_in_map_range(current_x - 1, current_y)):# and self.get_connected((current_y, current_x - 1)) == 0):
            ##33print("move West leagle, tring it!")
            move_west = (current_x - 1, current_y, )
            self.fill_connected_map(start_cell, move_west, connected_val, size)  # move west
            ##33print("moved west to ", move_west)
        return


    # def move(self, start_cell, current_cell, connected_val, size):
    #     for moves in range(LEGAL_ACTIONS):
    #         the_move = (current_cell[0] + moves[0], current_cell[1] + moves[1])
    #         if (self.is_in_map_range(the_move)):
    #             self.fill_connected_map(start_cell, the_move, connected_val, size)

    def connected_map_compleat_check(self):
        countZeros = 0
        for cellX in range(0, self.width()):
            for cellY in range(0, self.height()):  # shifts through columns of goal block
                # T = goalcellX, goalcellY
                # print("current check", T)
                if (not (self.get((cellX, cellY)) == 0)):
                    countZeros = countZeros + 1
                    print("Cell:" + cellX + "," + cellY + "has not been filled")
        print("total unfilled cells" + countZeros)
        return






    def block_fits(self, orgin, size, type):
        return True

#*******************************************************************************************************************
#*******************************************************************************************************************
#*******************************************************************************************************************
#*******************************************************************************************************************
#*******************************************************************************************************************






    # Student TODO: Replace this function with your A* implementation
    # returns a sample path from start tile to end tile which is probably illegal
    def get_path(self, start, end, size):
        path = []
        if(start == end):
            return path, 0, set()
        elif self.get_connected(start, size) > 0 and self.get_connected(start, size) == self.get_connected(end, size):
            search = A_Star(self)
            return A_Star.a_star_search(search, start, end, size)
        return path, 0, set()
        #44 if(self.is_connected(start, end, size)): #goal is the same traain as the
        #44
        #44     action = (1 if start[0] <= end[0] else -1, 1 if start[1] <= end[1] else -1)
        #44     d = (abs(start[0] - end[0]), abs(start[1] - end[1]))
        #44     # add the diagonal actions until we hit the row or column of the end tile
        #44     for diag in range(d[1] if d[0] > d[1] else d[0]):
        #44         #if(self.is_connected(start, action, size)):## not workign
        #44         path.append(action)
        #44         #else:
        #44         #   return path, 0, set()
        #44     # add the remaining straight actions to reach the end tile
        #44     for straight in range(d[0]-d[1] if d[0] > d[1] else d[1]-d[0]):
        #44         #if (self.is_connected(start, action, size)):## not workign
        #44         path.append((action[0], 0) if d[0] > d[1] else (0, action[1]))
        #44         #else:
        #44         #    return path, 0, set()
        #44     # return the path, the cost of the path, and the set of expanded nodes (for A*)
        #44     return path, sum(map(self.__get_action_cost, path)), set()
        #44 else:
        #44     return path,0,set()

    # Student TODO: Replace this function with a better (but admissible) heuristic
    # estimate the cost for moving between start and end
    def estimate_cost(self, start, goal):
        dinginal = DIAGONAL_COST*min(abs(numpy.subtract(start, goal)))+CARDINAL_COST*(max(abs(numpy.subtract(start, goal)))- min(abs(numpy.subtract(start,goal))))
        if self.is_connected(start, goal, 1):
            dist = DIAGONAL_COST*min(abs(numpy.subtract(start, goal)))+CARDINAL_COST*(max(abs(numpy.subtract(start, goal)))- min(abs(numpy.subtract(start,goal))))
            #((((start[1] - goal[1]) ** 2) + ((start[0] - goal[0]) ** 2)) ** (1 / 2)) * 100  # not done
            return math.ceil(dist)
        else:
            return 0

# Student TODO: You should implement AStar as a separate class
#               This will help keep things modular
class A_Star:
    # set up all the default values for the frid and read in the map from a given file
    def __init__(self, grid):
        # 2D list that will hold all of the grid tile information
        self.grid = grid
        self.connected_grid = grid.connected_grid
        self.h = 0
        self.f = 0
        self.open = []
        self.closed = set()
        self.path_cost = 0
        self.goal = (0, 0)


    def remove_min_from(self, list):
        #for n in range(len(self.open)):
        min_node = list[0]
        minvale = min_node.g
        for n in range(len(list)):
            if n.g < min_node.g:
                min_node = min(list)
                minvale = min_node.g
                pos = n
        #temp = min(list.remove(minvale)) #TODO this is not done it will not get the right value
        #temp = numpy.argmin(list)
        list.pop(pos)
        return minvale


    def expand(self, n, size):
        n_val = Grid.get_connected(self, n.tile, size)
        list = []
        # print("made it")
        for t in range(len(LEGAL_ACTIONS)):
            action = LEGAL_ACTIONS[t]
            move_to = (LEGAL_ACTIONS[t][0]+n.tile[0], LEGAL_ACTIONS[t][1]+n.tile[1])
            if Grid.is_in_map_range(self.grid, move_to[0], move_to[1]) and Grid.get_connected(self.grid, move_to, size) == n_val:
                # cost = self.grid.__get_action_cost(LEGAL_ACTIONS[t])
                if (abs(action[0] + action[1]) == 1):  # not dinginal
                    # print("made it 2 if ")
                    cost = CARDINAL_COST
                    new_node = Node(move_to, LEGAL_ACTIONS[t], cost, self.grid.estimate_cost(move_to, self.goal), n)
                    list.append(new_node)
                else:
                    # print("made it 2 else")
                    cost = DIAGONAL_COST
                    if(action[0] + action[1] == 2 and Grid.get_connected(self.grid, (move_to[0], move_to[1]-1), size) == n_val and Grid.get_connected(self.grid, (move_to[0]-1, move_to[1]), size) == n_val ): # south East
                        new_node = Node(move_to, LEGAL_ACTIONS[t], cost, self.grid.estimate_cost(move_to, self.goal), n)
                        list.append(new_node)
                    if (action[0] + action[1] == -2 and Grid.get_connected(self.grid, (move_to[0], move_to[1]+1), size) == n_val and Grid.get_connected(self.grid, (move_to[0]+1, move_to[1]), size) == n_val ): # North West
                        new_node = Node(move_to, LEGAL_ACTIONS[t], cost, self.grid.estimate_cost(move_to, self.goal), n)
                        list.append(new_node)
                    if (abs(action[0] + action[1]) == 0):
                        if (action[0] > action[1] and  Grid.get_connected(self.grid,(move_to[0], move_to[1] + 1),size) == n_val and Grid.get_connected(self.grid, (move_to[0] - 1, move_to[1]), size) == n_val): # North East
                            new_node = Node(move_to, LEGAL_ACTIONS[t], cost, self.grid.estimate_cost(move_to, self.goal), n)
                            list.append(new_node)
                        if(action[0] < action[1] and Grid.get_connected(self.grid,(move_to[0], move_to[1] - 1),size) == n_val and Grid.get_connected(self.grid, (move_to[0] + 1, move_to[1]), size) == n_val):# South West
                            new_node = Node(move_to, LEGAL_ACTIONS[t], cost, self.grid.estimate_cost(move_to, self.goal), n)
                            list.append(new_node)

                # def __init__(self, tile, action, action_cost, h, parent):
                # Node.define_node(new_node, self, n, move_to, cost, LEGAL_ACTIONS[t])

                # if move_to in self.open:
                #     #check if f/g val better
                #
                #     print("dont' add")
                # else:
                # list.append(new_node)
        return list

    def a_star_search(self, start, goal, size):
        heapq.heappush(self.open, Node(start, None, 0, self.grid.estimate_cost(start, goal), None))
        path = []
        self.path_cost = 0
        self.goal = goal

        while len(self.open) > 0:
            node = heapq.heappop(self.open)   # self.remove_min_from(self.open)

            #if node.tile in self.closed: continue
            if node.tile == self.goal:
                path, self.path_cost = self.reconstruct_path(node)
                # print("path", path, "\n path cost", self.path_cost)
                return path, self.path_cost, self.closed
            #print("tile added to closed set", node.tile)
            self.closed.add(node.tile)

            for child in self.expand(node, size):
                if child.tile in self.closed:
                    continue
                #  child.f = child.g + self.grid.estimate_cost(start, self.goal)

                if any((n.tile == child.tile and n.g <= child.g) for n in self.open):
                    continue

                heapq.heappush(self.open, child)

                # add_to_list, pos = self.not_in_list(child.tile, self.open)

                # if add_to_list:
                #     heapq.heappush(self.open, child)  # self.open.append(child)
                #     print("tile added to open", child.tile)
                # else:
                #     node = self.open[pos]
                #     if node.g <= child.g:
                #         continue
                #         # self.open[pos] = child
                #         # self.closed.add(node)
                #     elif node.g > child.g:  # node.f == child.f and
                #         # self.open[pos] = child
                #         self.open.remove(node)
                #         heapq.heappush(self.open, child)
                #         heapq.heapify(self.open)
                #         print("updated open", child.tile)
                #         # self.closed.add(node)
        print("exit")
        return path, self.path_cost, self.closed

    def not_in_list(self, tile, lis):
        for n in range(len(lis)):
            nod = lis[n]
            if tile == nod.tile:
                return False, n
            else:
                return True, 0
        return True, 0

    def reconstruct_path(self, goal_node):
        path = []
        node = goal_node
        p_cost =0
        # print("node", node, "node perenent", node)
        while node.parent: # != None:
            # print("node", node, "node perenent", node)
            path.insert(0, node.action)
            self.path_cost = +1
            if node.parent == node:
                # print("error node is it's own peret")
                return path
            if abs(node.action[0] + node.action[1]) == 1:  # not dinginal
                p_cost += CARDINAL_COST
            else:
                p_cost += DIAGONAL_COST
            node = node.parent
        # print("reconstruct_path exit")
        return path, p_cost





# Student TODO: You should implement a separate Node class
#               AStar search should use these Nodes in its open and closed lists
class Node:
    def __init__(self, tile, action, action_cost, h, parent):
        self.tile = tile
        self.action = action
        self.g = 0  # cost from start to here h i s the huistic
        self.parent = parent
        if (parent != None):
            self.g = parent.g + action_cost
        self.f = self.g + h  # h+g

   #  Node(start_state, None, 0, estimate_cost(start, goal), None)



    def __lt__(self, other):
        if self.f != other.f:
            return self.f < other.f
        else:
            return self.g > other.g

    def define_node(self, a_star, parent, tile, move_cost, move):
        self.parent = parent
        self.tile = tile
        self.action = move
        self.g = parent.g + move_cost
        self.f = self.g + Grid.estimate_cost(a_star.grid, tile, a_star.goal)
        # print("new node: f: ", self.g, " g ", self.f)
